import boto3
import json
import requests

s3 = boto3.client('s3')

# Useful Information
# You need to Package this function to upload it into AWS
# Use this guide
# https://docs.aws.amazon.com/lambda/latest/dg/python-package.html
# You need to package the library requests

def lambda_handler(event, context):
	# This value is from the file triggered the Lambda Function
	key = None
	try:
		data = get_params(key)
		data = get_data_cluster_values(data)
		data = get_params_deploy_spark(data)
		launch_spark(data)
	except Exception as e:
		print(e)
		raise e

def get_params(key):
	# Get the values from the file that triggered the lambda function



    # Set the values in the Dict Data
	data = {}
	data["team"] = None
	data["env"] = None
	return data

def get_data_cluster_values(data):
	# Set the values from the cluster and the rest of the values
	
	num_nodes = None
	num_cores_per_node = None
	mem_gb_per_code = None
	cores_per_executor = 5
	jar = None
	className = "nl.linkit.main.Main"
	url = "http://<EMR_URL>:8998"
	
    
    # Set the values in the Dict Data
	data["num_nodes"] = num_nodes
	data["num_cores_per_node"] = num_cores_per_node
	data["mem_gb_per_code"] = mem_gb_per_code
	data["cores_per_executor"] = cores_per_executor
	data["jar"] = jar
	data["className"] = className
	data["url"] = url

	return data

def get_params_deploy_spark(data):
	# Calculate the number of executors and memory per executor using the formula from the Lesson 4 Spark (Slide 36). 
	num_nodes = data["num_nodes"]
	num_cores_per_node = data["num_cores_per_node"]
	mem_gb_per_code = data["mem_gb_per_code"]
	cores_per_executor = data["cores_per_executor"]

	# Do the Math!



    # Set the values in the Dict Data
	data["executors"] = None
	data["executor_memory"] = None

	return data

def launch_spark(inputData):
	# Do the request to the Apache Livy's endpoint using the library requests
	# Check the documentation
	# https://livy.incubator.apache.org/docs/latest/rest-api.html

	# Spark Submit Example
	# spark-submit --master yarn --deploy-mode cluster --executor-cores 5 --executor-memory 19G --num-executors 11 --class nl.linkit.main.Main s3://linkit-spark-training-data/jars/Sparkify-assembly-0.1.jar --env qas

	# Add the correct url method for the Endpoint, check the documentation
	url = data["url"] + "/"

	# These args is going to be added in the data Dict, leave it like that
	args = [
		"--env",
		inputData["env"]
	]

	# Add the correct values for the POST Request
	# In the conf, Add the spark.submit.deployMode as cluster and spark.master as yarn
	data = {
		
	}

	
	# Create the Header for a Json Request
	headers = None
	

	# Do the POST Request using all the values, and print the response
	response = None


